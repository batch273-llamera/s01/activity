/*

    Part 1: Quiz
    
    Quiz:
    1. How do you create arrays in JS?
        
    const array_name = [item1, item2, ...];

    2. How do you access the first character of an array?
        
    str.charAt(0);

    3. How do you access the last character of an array?

    str.charAt(str.length - 1);

    4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
        
    indexOf() method

    5. What array method loops over all elements of an array, performing a user-defined function on each iteration?

    forEach() method

    6. What array method creates a new array with elements obtained from a user-defined function?

    map() method

    7. What array method checks if all its elements satisfy a given condition?

    every() method

    8. What array method checks if at least one of its elements satisfies a given condition?

    some() method

    9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.

    False

    10.True or False: array.slice() copies elements from original array and returns them as a new array.

    True

*/

// Part 2: Function Coding

let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// Number 1
function addToEnd(students, studentName) {
    if (typeof studentName === 'string') {
        students.push(studentName);
        return students;
    }
    else {
        return 'error - can only add strings to an array';
    }
}

// Number 2
function addToStart(students, studentName) {
    if (typeof studentName === 'string') {
        students.unshift(studentName);
        return students;
    }
    else {
        return 'error - can only add strings to an array';
    }
}

// Number 3
function elementChecker(students, studentName) {
    if (typeof studentName === 'string' && students.includes(studentName)) {
        return true;
    }
    else {
        return 'error - can only add strings to an array';
    }
}

// Number 4
function checkAllStringsEnding(students, stringEnding) {
    if (students.length === 0) {
        return 'error - array must NOT be empty';
    }
    else if (!students.every((student) => typeof student === "string")) {
        return 'error - all array elements must be strings';
    }
    else if (typeof stringEnding !== "string") {
            return "error - 2nd argument must be of data type string";
    }
    else if (stringEnding.length !== 1) {
            return "error - 2nd argument must be a single character";
    }
    else {
  	    return students.every((student) => student.endsWith(stringEnding));
    }
};

// Number 5
function stringLengthSorter(students) {
    if (students.every((student) => typeof student === "string")) {
        return students.sort((a, b) => a.length - b.length);
    }
    else {
        return 'error - all array elements must be string';
    }
};

// Number 6
function startsWithCounter(students, char) {

  	if (students.length === 0) {
    	return 'error - array must NOT be empty';
  	}

  	if (!students.every((student) => typeof student === "string")) {
    	return 'error - all array elements must be strings';
  	}

  	if (typeof char !== "string" || char.length !== 1) {
    	return 'error - 2nd argument must be a single character';
  	}

  	const count = students.filter((student) =>
    	student.toLowerCase().startsWith(char.toLowerCase())
  		).length;

  	return count;
};

